from collections import Counter
with open('trace.txt') as f:
    lines = f.readlines()

def line_to_info(l):
    if 'cksum' in l and '>' in l:
        source = l.strip().split(':')[0].split('>')[0].split('.')
        dest = l.strip().split(':')[0].split('>')[1].split('.')
        source_ip = '.'.join(source[:4])
        dest_ip = '.'.join(dest[:4])
        source_port = None if len(source) < 5 else source[4]
        dest_port = None if len(dest) < 5 else dest[4]
        d = dict([y for y in [x.strip().split() for x in l.split(',')] if len(y) == 2])
        if 'Flags' in l:
            flags = l[l.find('[') + 1:l.find(']')]
        else:
            flags = None
            
        seq_num = None if 'seq' not in d.keys() else d['seq']
        return tuple([x if x is None else x.strip() for x in (source_ip, source_port, dest_ip, dest_port, seq_num, flags)])
        

    
if __name__ == '__main__':
    # part 1 List 5 IP addresses of websites that it have been accessed by your mobile device. 10.30.22.101
    device_ip = '10.30.22.101'
    device_lines = [line_to_info(l) for l in lines if device_ip in l]
    #print device_lines[:10000]
    lines_parsed = [line_to_info(l) for l in lines]
    
    # port scanning 
    most_common_dest_ips = Counter([x[2] for x in lines_parsed if x is not None and 'R' in x[5]])
    print most_common_dest_ips # 10.30.1.65 scanner get's rejected many times 
    print Counter([x[2] for x in lines_parsed if x is not None and x[0] == '10.30.1.65'])
    # tells us that 10.30.5.234 is the scannee port
    attacked_ports = sorted([int(x[3]) for x in lines_parsed if x is not None and x[0] == '10.30.1.65' and x[2] == '10.30.5.234'])
    print attacked_ports[0], attacked_ports[-1]
    
    # syn packet identifying
    most_common_source_ips = Counter([x[0] for x in lines_parsed if x is not None])
    print most_common_source_ips
    # Attacker is 10.30.12.152, Victim is 10.30.17.255
    
    # packet injection
    #most_common_sequence_numbers = Counter([x[4] for x in lines_parsed if x is not None])
    #print most_common_sequence_numbers.most_common(100)
    
     