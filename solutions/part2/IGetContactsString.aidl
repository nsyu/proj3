// IGetContactsString.aidl
package com.cs155.trustedapp;

// Declare any non-default types here with import statements

interface IGetContactsString {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    String GetContacts(String paramAnonymousString);
}
